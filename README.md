# OpenML dataset: QSAR_fish_toxicity

https://www.openml.org/d/44815

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Data set containing values for 6 attributes (molecular descriptors) of 908 chemicals used to predict quantitative acute aquatic toxicity towards the fish Pimephales promelas (fathead minnow).

This dataset was used to develop quantitative regression QSAR models to predict acute aquatic toxicity towards the fish Pimephales promelas (fathead minnow) on a set of 908 chemicals. LC50 data, which is the concentration that causes death in 50% of test fish over a test duration of 96 hours, was used as model response.

**Attribute Description**

The model comprised 6 molecular descriptors

1. *CIC0* - information indices
2. *SM1_Dz* - 2D matrix-based descriptors
3. *GATS1i* - 2D autocorrelations
4. *NdsCH* - atom-type counts
5. *NdssC* - atom-type counts
6. *MLOGP* - molecular properties
7. *LC50* - quantitative response, LC50 [-LOG(mol/L)], target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44815) of an [OpenML dataset](https://www.openml.org/d/44815). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44815/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44815/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44815/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

